from audioop import mul
from glob import glob
import hypernetx as hnx #? https://github.com/pnnl/HyperNetX
# License
# =======

# HyperNetX

# Copyright © 2018, Battelle Memorial Institute

# Battelle Memorial Institute (hereinafter Battelle) hereby grants permission
# to any person or entity lawfully obtaining a copy of this software and associated
# documentation files (hereinafter “the Software”) to redistribute and use the
# Software in source and binary forms, with or without modification.  Such person
# or entity may use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and may permit others to do so, subject to the
# following conditions:

# -	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
# -	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other
# -	Other than as used herein, neither the name Battelle Memorial Institute or Battelle may be used in any form whatsoever without the express written consent of Battelle.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL BATTELLE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import matplotlib.pyplot as plt
import random
import copy
import sys

input = str("lfr_150_m03_on70_om2.dat")
weight_type = "constant"


# input = "hyperedges-test.txt"
# input = "lfr_150_m01_on13_om2_community.dat"
# input = "lfr_1000_m01_on100_om2_community.dat"


def get_second(tuple):
    return tuple[1]

def read_graph(file_name):
    f = open(file_name)
    lines = f.readlines()
    edges = {}
    for i,line in enumerate(lines):
        if "hyperedges" in file_name:
            edges[i] = line.strip().split(",")
        if "community" in file_name:
            edges[i] = line.strip().split("\t")[1].split(" ")
        elif "om2" in file_name:
            splitline = line.strip().split("\t")
            # print(splitline)
            if not int(splitline[1]) in edges.keys():
                edges[int(splitline[1])] = []
            edges[int(splitline[1])].append(splitline[0])
    print(edges)
    return hnx.Hypergraph(edges)


# H = read_graph("inputs/" + input)
# hnx.drawing.draw(H)
# plt.show()


node_percent = 0.1
# k_nodes = max(int(len(list(H.nodes)) * node_percent), 1)


# output = "outputs/2_" + input + "_" + str((sys.argv[2])) + "_" + weight_type + "_p_mut_" + str(mutation_chance) + "_pop_size_" + str(pop_size) + "_p_cross_" + str(p_cross) + "_tour_size_" + str(tournament_size) + ".txt"

current_cards = {}
current_multis = {}


def cardinality(edge_key, H2):
    # return len(H2.incidence_dict[edge_key])
    return current_cards[edge_key]

def multiplicity(edge_key, H2): #!.......................Nem lenne elég csak egyszer kiszámolni? Mivel megegyezéseket nézünk és ha kiveszünk egy pontot azt minden honnan kivesszük, így a maradék egyforma marad.
    # incidence = H2.incidence_dict
    # edge_value = incidence[edge_key]
    # m = 0
    # for edge in incidence.keys():
    #     if cardinality(edge, H2) == cardinality(edge_key, H2):
    #         if incidence[edge] == edge_value:
    #             m += 1
    # return m
    return current_multis[edge_key]

def weight(edge, H2):
    if weight_type == "constant":
        return 1
    if weight_type == "freq":
        return multiplicity(edge, H2)
    if weight_type == "Newman":
        if cardinality(edge, H2) == 1:
            return 0
        return multiplicity(edge, H2)/(cardinality(edge, H2) - 1)
    #? Gao mi a Beta?
    if weight_type == "network":
        if cardinality(edge, H2) == 1:
            return 0
        return 1 - pow(1 - (1/(cardinality(edge, H2) - 1)), multiplicity(edge, H2))


def weighted_node_degree_centrality(H2, node): #* final
    incidence = H2.incidence_dict
    res = 0
    for edge in incidence.keys():
        if node in incidence[edge]:
            res += weight(edge, H2) * len(incidence[edge])
    return res

def precalculate_values(H2):
    print("precalculating values")
    incidence = H2.incidence_dict
    global current_cards
    global current_multis
    current_cards = {}
    current_multis = {}
    for edge in incidence.keys():
        current_cards[edge] = len(incidence[edge])
    for edge in incidence.keys():
        m = 0
        for edge2 in incidence.keys():
            if cardinality(edge, H2) == cardinality(edge2, H2):
                if incidence[edge] == incidence[edge2]:
                    m += 1
        current_multis[edge] = m

def fitness(individual):
    # lower the average centrality by removing the nodes from individual
    H2 = copy.deepcopy(H)
    for node in individual:
        H2.remove_node(node)
    if weight_type != "constant":
        precalculate_values(H2)
    avg = 0
    for node in list(H2.nodes):
        # print(node)
        avg += weighted_node_degree_centrality(H2, node)
    avg /= float(len(list(H2.nodes)))
    return avg


def analyze_result(n):
    global weight_type
    global input
    total_nodes = {}
    diffs = 0

    for input in ["lfr_1000_m01_on100_om2_community.dat"]:#, "lfr_150_m03_on50_om2_community.dat", "lfr_1000_m01_on300_om2_community.dat", "lfr_1000_m02_on100_om2_community.dat"]:
        results = [[],[],[],[]]
        fig, axs = plt.subplots(2)
        greedy_res = [0,0,0,0]
        diff = [0,0,0,0]
        for q,weight_type in enumerate(['constant','freq','Newman','network']):
        
            results[q] = [0] * 200
            total_results = [0] * 200
        
        
            greedy_input = "outputs/greedy_" + input + "_" + str(0) + "_" + weight_type + ".txt"
            f = open(greedy_input, "r")
            lines = f.readlines()
            base = float(lines[0])
            # axs[q].plot([0, 200],[base,base], "r")
            greedy_res[q] = float(lines[1])
            # axs[q].plot([0, 200],[greedy_res[q],greedy_res[q]], "g")
            # print(base, greedy_res[q])
            f.close()
            for i in range(n):
                result_input = "outputs/2_" + input + "_" + str(i) + "_" + weight_type + "_p_mut_0.05_pop_size_50_p_cross_0.8_tour_size_3.txt"
                f = open(result_input, "r")
                lines = f.readlines()
                
                for l,line in enumerate(lines):
                    splitline = line.strip().replace("[","[ ").replace("]"," ]").split(" ")
                    # print(splitline)       
                    nodes = []
                    was = False

                    results[q][l] += (float(splitline[1]))
                    total_results[l] += (float(splitline[-1]))
                f.close()

    # print(results[1])


        # fig.suptitle('Best result and sum of all results in every generation for syn2') 
        #syn1 = lfr_150_m03_on70_om2.dat
        #syn2 = lfr_1000_m04_on500_om2_community.dat
        #syn3 = lfr_150_m02_on13_om2_community.dat
        for q in range(4):
            results[q] = [i/10 for i in results[q]]
            diff[q] += 100 - ((results[q][199]*100 )/ greedy_res[q])
        print(diff)
        print(sum(diff)/4)
        diffs += sum(diff) / 4
    # total_results = [i/60 for i in total_results]
    # plt.plot(results)
        fig, axs = plt.subplots(2)
        axs[0].plot(results[0])
        axs[0].plot([0, 200],[greedy_res[0],greedy_res[0]], "g")
        axs[1].plot(results[1])
        axs[1].plot([0, 200],[greedy_res[1],greedy_res[1]], "g")
        plt.show()
        fig, axs = plt.subplots(2)
        axs[0].plot(results[2])
        axs[0].plot([0, 200],[greedy_res[2],greedy_res[2]], "g")
        axs[1].plot(results[3])
        axs[1].plot([0, 200],[greedy_res[3],greedy_res[3]], "g")

        plt.show()
    
    print(diffs / 4)
            # line = lines[-1]
            # nodes = []
            # splitline = line.strip().replace("[","[ ").replace("]"," ]").split(" ")
            # for j,item in enumerate(splitline):
            #     if j == 1:
            #         results.append(float(item))
            #     if j >= 3 and j < 3 + k_nodes:
            #         nodes.append(item)
            #     if j > 3 + k_nodes:
            #         total_results.append(float(item))
            
            # new_nodes = []
            # for node in nodes:
            #     node = node.replace(",","").replace("\'","")
            #     new_nodes.append(str(node))
            # nodes = new_nodes
            # print(sorted(nodes))
            # old_weight = weight_type
            # weight_type = 'constant'
            # print(fitness(nodes))
            # weight_type = 'freq'
            # print(fitness(nodes))
            # weight_type = 'Newman'
            # print(fitness(nodes))
            # weight_type = 'network'
            # print(fitness(nodes))
            # for node in nodes:
            #     if not node in total_nodes.keys():
            #         total_nodes[node] = 1
            #     else:
            #         total_nodes[node] += 1


    # best_nodes = []
    # for node in total_nodes.keys():
    #     if total_nodes[node] >= 30:
    #         best_nodes.append(int(node))    
    # print(sorted(best_nodes))
    # print(len(best_nodes))

analyze_result(10)

# print(multiplicity(17, H))

# print(list(H.nodes))
# print(H.incidence_dict)
# print(fitness(['1']))
# print(weighted_node_degree_centrality(H,'2'))