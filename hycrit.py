from audioop import mul
from glob import glob
import hypernetx as hnx #? https://github.com/pnnl/HyperNetX
# License
# =======

# HyperNetX

# Copyright © 2018, Battelle Memorial Institute

# Battelle Memorial Institute (hereinafter Battelle) hereby grants permission
# to any person or entity lawfully obtaining a copy of this software and associated
# documentation files (hereinafter “the Software”) to redistribute and use the
# Software in source and binary forms, with or without modification.  Such person
# or entity may use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and may permit others to do so, subject to the
# following conditions:

# -	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
# -	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other
# -	Other than as used herein, neither the name Battelle Memorial Institute or Battelle may be used in any form whatsoever without the express written consent of Battelle.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL BATTELLE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import matplotlib.pyplot as plt
import random
import copy
import sys

input = str(sys.argv[1])
weight_type = str(sys.argv[3])
# input = "hyperedges-test.txt"
# input = "lfr_150_m01_on13_om2_community.dat"
# input = "lfr_1000_m01_on100_om2_community.dat"


def get_second(tuple):
    return tuple[1]

def read_graph(file_name):
    f = open(file_name)
    lines = f.readlines()
    edges = {}
    for i,line in enumerate(lines):
        if "hyperedges" in file_name:
            edges[i] = line.strip().split(",")
        if "community" in file_name:
            edges[i] = line.strip().split("\t")[1].split(" ")
        elif "om2" in file_name:
            splitline = line.strip().split("\t")
            # print(splitline)
            if not int(splitline[1]) in edges.keys():
                edges[int(splitline[1])] = []
            edges[int(splitline[1])].append(splitline[0])
    print(edges)
    return hnx.Hypergraph(edges)


H = read_graph("inputs/" + input)
# hnx.drawing.draw(H)
# plt.show()

gen_count = 200
mutation_chance = float(sys.argv[4]) #0.05
node_percent = 0.1
k_nodes = max(int(len(list(H.nodes)) * node_percent), 1)
pop_size = int(sys.argv[5])
p_cross = float(sys.argv[6])
tournament_round_count = int(pop_size * p_cross * 0.5) # number of rounds in tournament
tournament_size = int(sys.argv[7])

output = "outputs/2_" + input + "_" + str((sys.argv[2])) + "_" + weight_type + "_p_mut_" + str(mutation_chance) + "_pop_size_" + str(pop_size) + "_p_cross_" + str(p_cross) + "_tour_size_" + str(tournament_size) + ".txt"
output_greedy = "outputs/greedy_" + input + "_" + str((sys.argv[2])) + "_" + weight_type + ".txt"

current_cards = {}
current_multis = {}

def generate_pop(): #* final 
    pop = []
    for _ in range(pop_size):
        pop.append(random.sample(list(H.nodes), k_nodes))
    return pop

def cardinality(edge_key, H2):
    # return len(H2.incidence_dict[edge_key])
    return current_cards[edge_key]

def multiplicity(edge_key, H2): #!.......................Nem lenne elég csak egyszer kiszámolni? Mivel megegyezéseket nézünk és ha kiveszünk egy pontot azt minden honnan kivesszük, így a maradék egyforma marad.
    # incidence = H2.incidence_dict
    # edge_value = incidence[edge_key]
    # m = 0
    # for edge in incidence.keys():
    #     if cardinality(edge, H2) == cardinality(edge_key, H2):
    #         if incidence[edge] == edge_value:
    #             m += 1
    # return m
    return current_multis[edge_key]

def weight(edge, H2):
    if weight_type == "constant":
        return 1
    if weight_type == "freq":
        return multiplicity(edge, H2)
    if weight_type == "Newman":
        if cardinality(edge, H2) == 1:
            return 0
        return multiplicity(edge, H2)/(cardinality(edge, H2) - 1)
    #? Gao mi a Beta?
    if weight_type == "network":
        if cardinality(edge, H2) == 1:
            return 0
        return 1 - pow(1 - (1/(cardinality(edge, H2) - 1)), multiplicity(edge, H2))


def weighted_node_degree_centrality(H2, node): #* final
    incidence = H2.incidence_dict
    res = 0
    for edge in incidence.keys():
        if node in incidence[edge]:
            res += weight(edge, H2) * len(incidence[edge])
    return res

def precalculate_values(H2):
    print("precalculating values")
    incidence = H2.incidence_dict
    global current_cards
    global current_multis
    current_cards = {}
    current_multis = {}
    for edge in incidence.keys():
        current_cards[edge] = len(incidence[edge])
    for edge in incidence.keys():
        m = 0
        for edge2 in incidence.keys():
            if cardinality(edge, H2) == cardinality(edge2, H2):
                if incidence[edge] == incidence[edge2]:
                    m += 1
        current_multis[edge] = m

def fitness(individual):
    # lower the average centrality by removing the nodes from individual
    H2 = copy.deepcopy(H)
    for node in individual:
        H2.remove_node(node)
    if weight_type != "constant":
        precalculate_values(H2)
    avg = 0
    for node in list(H2.nodes):
        # print(node)
        avg += weighted_node_degree_centrality(H2, node)
    avg /= float(len(list(H2.nodes)))
    return avg

def split_node_lists(united_node_list):
    first_child_nodes = []
    second_child_nodes = []
    # print(united_node_list)
    random.shuffle(united_node_list)
    print(united_node_list)
    new_united_node_list = []

    for node in united_node_list:
        if united_node_list.count(node) == 1:
            new_united_node_list.append(node)
        else:
            if not node in first_child_nodes:
                first_child_nodes.append(node)
            if not node in second_child_nodes:
                second_child_nodes.append(node)
    united_node_list = new_united_node_list.copy()
    print((first_child_nodes, second_child_nodes))
    print(united_node_list)
    for node in united_node_list: 
        if not node in first_child_nodes and len(first_child_nodes) < k_nodes:
            first_child_nodes.append(node)
        elif not node in second_child_nodes:
            second_child_nodes.append(node)
    print((first_child_nodes, second_child_nodes))
    return first_child_nodes, second_child_nodes

def find_min_from_contenders(contenders):
    min_val1, min_val2 = float('inf'), float('inf')
    min1, min2 = None, None
    for contender in contenders:
        if contender[1] < min_val2:
            min2 = copy.deepcopy(min1)
            min_val2 = min_val1
            min1 = copy.deepcopy(contender)
            min_val1 = contender[1]
    return [min1, min2]


def tournament_round(evaluated_population):
    current_contenders = random.sample(evaluated_population, tournament_size)
    current_contenders = find_min_from_contenders(current_contenders)
    # print(current_contenders)

    united_node_list = current_contenders[0][0] + current_contenders[1][0]
    first_child_nodes, second_child_nodes = split_node_lists(united_node_list)

    first_child = first_child_nodes
    second_child = second_child_nodes
    return first_child, second_child

def crossover_tournament(evaluated_population):
    print("Tournament")
    evaluated_child_population = []
    for _ in range(tournament_round_count):
        first_child, second_child = tournament_round(evaluated_population)
        evaluated_child_population.append((copy.deepcopy(first_child), fitness(first_child)))
        evaluated_child_population.append((copy.deepcopy(second_child), fitness(second_child)))
    
    return evaluated_child_population

def mutate(evaluated_individual): #* final
    individual = copy.deepcopy(evaluated_individual[0])
    chosen_node = random.choice(individual)
    individual.remove(chosen_node)
    new_node = random.choice(list(H.nodes))
    while new_node in individual:
        new_node = random.choice(list(H.nodes))
    individual.append(new_node)

    return (individual, fitness(individual))

def selection(evaluated_population): #* final
    print("Selection")
    new_evaluated_population = evaluated_population.copy()
    new_evaluated_population = sorted(new_evaluated_population, key=get_second)
    new_evaluated_population = new_evaluated_population[0:pop_size]
    return new_evaluated_population

def ga():
    print("Starting GA")
    population = generate_pop()
    evaluated_population = [(individual, fitness(individual)) for individual in population]
    print(population)
    f = open(output, "w+")
    for current_gen in range(gen_count):
        evaluated_child_population = crossover_tournament(evaluated_population)

        if random.random() < mutation_chance:
            original_individual = random.choice(evaluated_child_population)
            mutating_individual = mutate(original_individual)
            evaluated_child_population.remove(original_individual)
            evaluated_child_population.append(mutating_individual)

        evaluated_population = evaluated_population + evaluated_child_population

        evaluated_population = selection(evaluated_population)
        
        total = 0
        for evaluated_individual in evaluated_population:
            total += evaluated_individual[1]
        print(str(current_gen) + " " + str(evaluated_population[0][1]) + " " + str(evaluated_population[0][0]) + " " + str(total), file=f)
        print(str(current_gen) + " " + str(evaluated_population[0][1]) + " " + str(evaluated_population[0][0]) + " " + str(total))
        print(evaluated_population)
        print("Currently at gen: " + str(current_gen))


def greedy():
    print("Starting Greedy")
    f = open(output_greedy, "w+")
    global current_multis
    global current_cards
    precalculate_values(H)
    centralities = []
    for i in H.nodes:
        centralities.append((i, weighted_node_degree_centrality(H, i)))
    # average = sum(centralities.values()) / len(centralities.values())
    centralities.sort(key = lambda x : x[1], reverse = True)
    best_cents = list(zip(*centralities[0:k_nodes]))[0]
    print(sum(list(zip(*centralities))[1]) / len(H.nodes), file = f)
    H2 = copy.deepcopy(H)
    for node in best_cents:
        H2.remove_node(node)
    current_multis = {}
    current_cards = {}
    precalculate_values(H2)
    centralities = []
    for i in H2.nodes:
        centralities.append((i, weighted_node_degree_centrality(H2, i)))
    avg = sum(list(zip(*centralities))[1]) / len(H2.nodes)

    print(str(avg) + "\n" + str(best_cents), file=f)
    f.close()

# ga()

greedy()


# print(multiplicity(17, H))

# print(list(H.nodes))
# print(H.incidence_dict)
# print(fitness(['1']))
# print(weighted_node_degree_centrality(H,'2'))