from audioop import avg, mul
from glob import glob
import hypernetx as hnx #? https://github.com/pnnl/HyperNetX
# License
# =======

# HyperNetX

# Copyright © 2018, Battelle Memorial Institute

# Battelle Memorial Institute (hereinafter Battelle) hereby grants permission
# to any person or entity lawfully obtaining a copy of this software and associated
# documentation files (hereinafter “the Software”) to redistribute and use the
# Software in source and binary forms, with or without modification.  Such person
# or entity may use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and may permit others to do so, subject to the
# following conditions:

# -	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
# -	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other
# -	Other than as used herein, neither the name Battelle Memorial Institute or Battelle may be used in any form whatsoever without the express written consent of Battelle.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL BATTELLE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import matplotlib.pyplot as plt
import random
import copy
import sys

input = "lfr_1000_m01_on500_om2_community.dat"



def get_second(tuple):
    return tuple[1]

def read_graph(file_name):
    f = open(file_name)
    lines = f.readlines()
    edges = {}
    for i,line in enumerate(lines):
        if "hyperedges" in file_name:
            edges[i] = line.strip().split(",")
        if "community" in file_name:
            edges[i] = line.strip().split("\t")[1].split(" ")
    # print(edges)
    return hnx.Hypergraph(edges)


def avg(a):
    return sum(a) / len(a)

def mean(a):
    a = sorted(a)
    return a[int(len(a)/2)]

H = read_graph("inputs/" + input)
print("|X| - " + str(len(list(H.nodes))))
print("|D| - " + str(len(list(H.edges))))
X = H.incidence_dict
L = [len(i) for i in X.values()]
# X.values
print("rank - " + str(max(L)))
print("avg - " + str(avg(L)))
print("mean - " + str(mean(L)))
# hnx.drawing.draw(H)
# plt.show()

