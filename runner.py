import subprocess

file_list = ["lfr_1000_m01_on100_om2_community.dat", "lfr_150_m03_on50_om2_community.dat", "lfr_1000_m01_on300_om2_community.dat", "lfr_1000_m02_on100_om2_community.dat" ]
weight_types = ["constant","freq","Newman","network"]
# gen_count = 200
mutation_chances = [0.05]
# node_percent = 0.1
pop_sizes = [50]
p_crosses = [0.8]
# tournament_round_count = int(pop_size * p_cross * 0.5) # number of rounds in tournament
tournament_sizes = [3]

for file in file_list:
    for weight_type in weight_types:
        for mutation_chance in mutation_chances:
            for pop_size in pop_sizes:
                for p_cross in p_crosses:
                    for tournament_size in tournament_sizes:
                        for i in range(1):
                            list_dir = subprocess.Popen(["python", "hycrit.py", file, str(i), str(weight_type), str(mutation_chance), str(pop_size), str(p_cross), str(tournament_size)])
                            list_dir.wait()
